<?php
$title = 'Список полдьзователей';
$menu = 'user';
?>

<?php include '../resources/views/layout/top.php' ?>

    <div class="container">
        <div class="row">
            <div class="col-lg-12">

                <?php if (app('request')->get('success') !== null) : ?>
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        Данные пользователя успешно сохранены
                    </div>
                <?php endif; ?>

                <form class="form-horizontal" method="post" action="<?= route('admin_user_update', ['id' => $user->id]) ?>">
                    <input type="hidden" name="id" value="<?= $user->id ?>">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Email</label>
                        <div class="col-sm-8">
                            <input type="text" name="email" class="form-control" value="<?= $user->email ?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Имя</label>
                        <div class="col-sm-8">
                            <input type="text" name="name" class="form-control" value="<?= $user->name ?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Пароль</label>
                        <div class="col-sm-8">
                            <input type="text" name="password" class="form-control" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Роль</label>
                        <div class="col-sm-8">
                        <select class="form-control" name="role_id">
                            <?php foreach ($role as $value) : ?>
                                <option value="<?= $value->id ?>" <?= ($user->role_id == $value->id) ? 'selected' : '' ?>><?= $value->name ?></option>
                            <?php endforeach; ?>
                        </select>
                        </div>
                    </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Пароль</label>
                            <div class="col-sm-8">
                        <select class="form-control" name="category_id">
                            <option value="">Укажите рубрику</option>
                            <?php foreach ($category as $value) : ?>
                                <option value="<?= $value->id ?>" <?= ($user->category_id == $value->id) ? 'selected' : '' ?>><?= $value->name ?></option>
                            <?php endforeach; ?>
                        </select>
                            </div>
                        </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-8">
                            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Сохранить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php include '../resources/views/layout/button.php'; ?>