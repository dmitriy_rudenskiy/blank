<?php

namespace App\Util;

class Phone
{
    public static function clear($text)
    {
        return (int)preg_replace("/[^0-9]/", "", $text);
    }
}