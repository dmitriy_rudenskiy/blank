<?php
namespace App\Util;

use Imagine\Image\Box;

class ImageUtil
{
    const MIDDLE_WIDTH = 300;
    const MIDDLE_HEIGHT = 240;

    const SMALL_WIDTH = 80;
    const SMALL_HEIGHT = 60;

    const PREFIX_NET = 'net:';
    const PREFIX_UPLOAD = 'upload:';

    public function createUrl($hash, $isSmall)
    {
        if ($isSmall) {
            $size = sprintf("%dx%d", self::SMALL_WIDTH, self::SMALL_HEIGHT);
        } else {
            $size = sprintf("%dx%d", self::MIDDLE_WIDTH, self::MIDDLE_HEIGHT);
        }

        return sprintf(
            "%s/%s/%s/%s/%s.jpeg",
            '/media',
            $size,
            substr($hash, 0, 2),
            substr($hash, 2, 2),
            $hash
        );
    }

    public function getMiddleBox()
    {
        return new Box(self::MIDDLE_WIDTH, self::MIDDLE_HEIGHT);
    }

    public function getSmallBox()
    {
        return new Box(self::SMALL_WIDTH, self::SMALL_HEIGHT);
    }

    public function createName($string, $isLoad = false)
    {
        if ($isLoad) {
            $prefix = self::PREFIX_NET;
        } else {
            $prefix = self::PREFIX_UPLOAD;
        }

        return md5($prefix . $string);
    }
}