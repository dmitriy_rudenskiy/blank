<?php
use Illuminate\Support\Facades\Route;

Route::get('unsubscribe/{token}', 'Front\BidderController@unsubscribe')->name('unsubscribe');
Route::get('verification/error', 'Auth\AuthController@getVerificationError')->name('verification_error');
Route::get('verification/success', 'HomeController@getVerificationSuccess')->name('verification_success');
Route::get('verification/{token}', 'Auth\AuthController@getVerification')->name('verification');
Route::get('register/success', 'Auth\AuthController@success')->name('register_success');
Route::auth();

Route::group(['namespace' => 'Front',  'as' => 'front_'], function () {
    Route::get('/', 'HomeController@index')->name('home');

    // производитель машин
    Route::get('catalog/price/{id}', 'ProductController@view')->name('catalog_price_view');
    Route::get('catalog/{maker}', 'CatalogController@maker')->name('catalog_maker');
    Route::get('catalog/{maker}/{model}', 'CatalogController@model')->name('catalog_model');

    Route::get('search', 'SearchController@index')->name('search_index');

    // корзина
    Route::get('cart', 'CartController@index')->name('cart_index');
    Route::get('cart/order', 'CartController@order')->name('cart_order');
    Route::get('buy/{id}', 'BuyController@add')->name('cart_add');
    Route::get('buy/remove/{id}', 'BuyController@remove')->name('cart_remove');
});

Route::group(['namespace' => 'Admin', 'middleware' => ['web'],  'prefix' => '/admin', 'as' => 'admin_'], function () {
    Route::get('/', ['uses' => 'IndexController@index'])->name('root');

    // товары
    Route::get('product', 'ProductController@index')->name('product_index');
    Route::get('product/view/{id}', 'ProductController@view')->name('product_view');
    Route::post('product/update', 'ProductController@update')->name('product_update');
    Route::get('product/create', 'ProductController@create')->name('product_create');
    Route::post('product/insert', 'ProductController@insert')->name('product_insert');

    // картинки товаров
    Route::post('image/upload', 'ImageController@upload')->name('image_upload');
    Route::get('image/delete/{productId}/{imageId}', 'ImageController@delete')->name('image_delete');
    Route::post('image/set/thumbnail', 'ImageController@thumbnail')->name('image_set_thumbnail');

    Route::get('buy', 'CityController@index')->name('buy_index');
    Route::get('info', 'CityController@index')->name('info_index');
});