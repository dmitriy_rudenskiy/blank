<?php
namespace App\Http\Controllers\Front;

use App\Repositories\MakerRepository;
use Illuminate\Routing\Controller;

class HomeController extends Controller
{
    public function index(MakerRepository $repository)
    {
        $list = $repository->getList();

        // var_dump($list);

        return view('front.home.index');
    }

    public function search()
    {
        return view('front.home.about');
    }

    public function about()
    {
        return view('front.home.about');
    }
}
