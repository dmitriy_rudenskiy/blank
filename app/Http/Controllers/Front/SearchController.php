<?php
namespace App\Http\Controllers\Front;

use App\Entities\Product;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class SearchController extends Controller
{
    public function index(Request $request)
    {
        $query = $request->get('q');
        $error = $request->get('e');

        if (empty($query)) {
            return view('front.search.empty');
        }


        $listWord = explode(' ', $query);

        $paginate = Product::whereIn('maker', $listWord)
            ->paginate()
            ->appends(['q' => $query, 'e' => $error]);

        return view(
            'front.search.result',
            [
                'query' => $query,
                'error' => $error,
                'paginate' => $paginate
            ]
        );
    }

    public function search()
    {
        return view('front.home.about');
    }

    public function about()
    {
        return view('front.home.about');
    }
}
