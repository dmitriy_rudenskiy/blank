<?php
namespace App\Http\Controllers\Front;

use App\Entities\Product;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class ProductController extends Controller
{
    const SESSION_KEY = 'viewed_products';
    const LIMIT_HISTORY = '';


    public function view(ProductRepository $repository, $id)
    {
        // товар
        $product = $repository->find($id);

        // добавляем в список просмотренных
        $this->addViewedProducts($product);

        // получаем список просмотренных
        $productsId = array_keys($this->getViewedProducts());
        $viewed = $repository->findWhereIn('id', $productsId);

        return view(
            'front.product.view',
            [
                'product' => $product,
                'viewed' => $viewed
            ]
        );
    }

    protected function getViewedProducts()
    {
        return (array)session(self::SESSION_KEY);
    }

    protected function addViewedProducts(Product $product)
    {
        $id = (int)$product->id;

        $list = $this->getViewedProducts();

        $list[$id] = time();

        // сортируем по дате посещения
        arsort($list);

        $list = array_slice($list, 0, 5, true);

        session()->put(self::SESSION_KEY, $list);
    }

}
