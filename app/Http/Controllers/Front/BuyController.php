<?php
namespace App\Http\Controllers\Front;

use App\Components\Cart;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class BuyController extends Controller
{
    public function add($id, Request $request, ProductRepository $productRepository, Cart $cart)
    {
        $referrer = url()->previous();
        session()->put('referrer', $referrer);

        $product = $productRepository->find($id);

        if ($product !== null) {
            $cart->add($product);
        }

        return redirect()->route('front_cart_index');
    }

    public function remove($id, ProductRepository $productRepository, Cart $cart)
    {
        $product = $productRepository->find($id);

        if ($product !== null) {
            $cart->remove($product);
        }

        return redirect()->route('front_cart_index');
    }

    public function clear($id)
    {

    }

}
