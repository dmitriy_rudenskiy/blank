<?php
namespace App\Http\Controllers\Front;

use App\Components\Cart;
use App\Repositories\ProductRepository;
use Illuminate\Routing\Controller;

class CartController extends Controller
{
    public function index(ProductRepository $productRepository, Cart $cart)
    {
        $referrer = session('referrer');

        if (empty($referrer)) {
            $referrer = url()->previous();

            var_dump($referrer);
            exit();

            session()->put('referrer', $referrer);
        }

        $productsId = array_keys($cart->get());

        $list = $productRepository->findWhereIn('id', $productsId);
        $sum = $productRepository->findWhereIn('id', $productsId)->sum('price');

        return view(
            'front.cart.view',
            [
                'referrer' => $referrer,
                'list' => $list,
                'sum' => $sum
            ]
        );
    }

    public function order()
    {
        return view('front.cart.order');
    }
}
