<?php
namespace App\Http\Controllers\Admin;

use App\Entities\ProductImage;
use Illuminate\Http\Request;
use App\Repositories\ImageRepository;
use App\Repositories\ProductRepository;
use App\Util\ImageUtil;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Routing\Controller;
use Imagine\Gd\Imagine;
use Imagine\Image\ImageInterface;

class ImageController extends Controller
{
    public function upload(
        Request $request,
        ProductRepository $productRepository,
        ImageRepository $imageRepository)
    {
        $imageUtil = new ImageUtil();

        $productId = $request->get('product_id');
        $product = $productRepository->find($productId);

        $file = $request->file('image');
        $file->getClientOriginalName();

        // создаём имя
        $hash = $imageUtil->createName(microtime() . $file->getClientOriginalName());

        // создаём запись об изображение
        $image = $imageRepository->create(['hash' => $hash]);

        // прикрепляем к товару
        $productRepository->addImage($product, $image);

        // сохраняем изображение
        $thumbnail = (new Imagine())->open($file->path());

        // средний размер
        $filename =  base_path('public_html') . $imageUtil->createUrl($hash, false);
        $directory = dirname($filename);

        if (realpath($directory) === false) {
            mkdir($directory, 0755, true);
        }

        $thumbnail->thumbnail(
            $imageUtil->getMiddleBox(),
            ImageInterface::THUMBNAIL_INSET
        )->save($filename);

        // минимальный
        $filename =  base_path('public_html') . $imageUtil->createUrl($hash, true);
        $directory = dirname($filename);

        if (realpath($directory) === false) {
            mkdir($directory, 0755, true);
        }

        $thumbnail->thumbnail(
            $imageUtil->getSmallBox(),
            ImageInterface::THUMBNAIL_INSET
        )->save($filename);

        // Session::flash('key', 'value');

        return redirect()->back();
    }

    public function delete($productId, $imageId)
    {
        $productImage = ProductImage::where('product_id', $productId)
            ->where('image_id', $imageId)
            ->first();

        if ($productImage !== null) {
            $productImage->delete();
        }

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @param ProductRepository $repository
     */
    public function thumbnail(Request $request, ProductRepository $repository)
    {
        $append = !empty($request->get('status'));
        $productId = (int)$request->get('product_id');
        $imageId = (int)$request->get('image_id');

        // хочет отключить обложку, так делать нельзя
        if ($append) {
            $repository->setThumbnail($productId, $imageId);
        }

        return redirect()->back();
    }
}
