<?php
namespace App\Http\Controllers\Admin;

use App\Repositories\ProductRepository;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class ProductController extends Controller
{
    public function index(ProductRepository $repository, Request $request)
    {
        $query = $request->get('q');
        $list = $repository->getListForAdmin($query);

        return view(
            'admin.product.index',
            [
                'query' => $query,
                'list' => $list
            ]
        );
    }

    public function create(ProductRepository $repository)
    {
        $product = $repository->getNew();

        return view(
            'admin.product.view',
            [
                'product' => $product
            ]
        );
    }

    public function insert(ProductRepository $repository, Request $request)
    {
        $data = $request->only(['maker', 'model', 'part', 'code', 'price']);
        $product = $repository->create($data);

        return redirect()->route('admin_product_view', ['id' => $product->id]);
    }


    public function view(ProductRepository $repository, $id)
    {
        $product = $repository->find($id);

        return view(
            'admin.product.view',
            [
                'product' => $product
            ]
        );
    }

    public function update(ProductRepository $repository, Request $request)
    {
        $id = $request->get('id');
        $data = $request->only(['maker', 'model', 'part', 'code', 'price']);
        $repository->update($data, $id);

        return redirect()->back();
    }
}
