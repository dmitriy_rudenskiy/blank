<?php
namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\Order;
use App\Models\Role;
use App\User as User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class OrderCheckController extends Controller
{
    public function index()
    {
        $list = Order::orderBy('id')
            ->where('visible', '=', Order::STATE_ADDED)
            ->paginate();

        return view(
            'admin.check.order',
            [
                'list' => $list
            ]
        );
    }

    public function check($id)
    {
        $order = Order::find($id);

        if ($order !== null) {
            $order->visible = Order::STATE_CHECK;
            $order->save();
        }

        return redirect()->back();
    }

    public function cancel($id)
    {
        $order = Order::find($id);

        if ($order !== null) {
            $order->visible = Order::STATE_CANCEL;
            $order->save();
        }

        return redirect()->back();
    }
}
