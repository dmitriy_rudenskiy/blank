<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Product extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'product';

    protected $fillable = ['maker', 'model', 'part', 'code', 'price'];

    /**
     * @var int
     */
    private $thumbnail = -1;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->maker . ' ' . $this->model . ' ' . $this->part;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function images()
    {
        return $this->hasMany(ProductImage::class, 'product_id')
            ->orderBy('thumbnail', 'DESC')
            ->with('image');
    }

    /**
     * @return int|mixed
     */
    public function thumbnail()
    {
        if ($this->thumbnail === -1) {
            $this->thumbnail = $this->images()->first()->image;
        }

        return $this->thumbnail;
    }

}
