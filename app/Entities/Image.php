<?php

namespace App\Entities;

use App\Util\ImageUtil;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Image extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'image';

    protected $fillable = ['hash'];

    public $timestamps = false;

    public function getUrl()
    {
        $util = new ImageUtil();
        return $util->createUrl($this->hash, false);
    }

    public function getThumbnail()
    {
        $util = new ImageUtil();
        return $util->createUrl($this->hash, true);
    }
}
