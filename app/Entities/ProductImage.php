<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class ProductImage extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'product_image';

    protected $fillable = ['product_id', 'image_id'];

    protected $primaryKey = ['product_id', 'image_id'];

    public $incrementing = false;

    public $timestamps = false;

    protected function setKeysForSaveQuery(Builder $query)
    {

        foreach ($this->getKeyName() as $key) {
            if ($this->$key) {
                $query->where($key, '=', $this->$key);
            } else {
                throw new \Exception(__METHOD__ . 'Missing part of the primary key: ' . $key);
            }
        }

        return $query;
    }

    public function image()
    {
        return $this->belongsTo(Image::class, 'image_id');
    }
}
