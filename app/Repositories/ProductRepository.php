<?php

namespace App\Repositories;

use App\Entities\Image;
use App\Entities\Product;
use App\Entities\ProductImage;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class PostRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ProductRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Product::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getListForAdmin($query)
    {
        $queryBuilder = $this->with('images')->orderBy('id', 'DESC');

        if (!empty($query)) {
            $queryBuilder->findWhere([['maker', 'like', '%' . $query . '%']]);
        }

        return $queryBuilder->paginate(10);
    }

    /**
     * @param Product $product
     * @param Image $image
     */
    public function addImage(Product $product, Image $image)
    {
        $productImage = new ProductImage();
        $productImage->product_id = $product->id;
        $productImage->image_id = $image->id;
        $productImage->save();
    }

    /**
     * Устанавливаем обложку изображения
     *
     * @param $productId
     * @param $imageId
     * @return bool
     */
    public function setThumbnail($productId, $imageId)
    {
        $productImage = ProductImage::where('product_id', $productId)
            ->where('image_id', $imageId)
            ->first();

        // картинка не привязана к товару
        if ($productImage === null) {
            return false;
        }

        ProductImage::where('product_id', $productId)
            ->update(['thumbnail' => false]);

        $productImage->thumbnail = true;
        $productImage->save();

        return true;
    }

    public function getNew()
    {
        return new $this->model();
    }
}
