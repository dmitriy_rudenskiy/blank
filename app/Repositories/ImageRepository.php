<?php

namespace App\Repositories;

use App\Entities\Image;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class PostRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ImageRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Image::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getListForAdmin($query)
    {
        $queryBuilder = $this->orderBy('id', 'DESC');

        if (!empty($query)) {
            $queryBuilder->findWhere([['maker', 'like', '%' . $query . '%']]);
        }

        return $queryBuilder->paginate(10);
    }
}
