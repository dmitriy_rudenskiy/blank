<?php

namespace App\Repositories;

use App\Entities\Image;
use App\Entities\Product;
use App\Entities\ProductImage;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class PostRepositoryEloquent
 * @package namespace App\Repositories;
 */
class MakerRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Product::class;
    }

    public function getList()
    {
        return Product::groupBy('maker')
            ->lists('maker');
            //->get();
    }
}
