<?php

namespace App\Console\Commands;

use App\Entities\Image;
use App\Entities\Product;
use App\Entities\ProductImage;
use App\Util\ImageUtil;
use Illuminate\Console\Command;
use Imagine\Gd\Imagine;
use Imagine\Image\ImageInterface;
use DirectoryIterator;
use XMLReader;

class ImportDrom extends Command
{
    const EXTENSION = 'xml';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:drom';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $directory = storage_path(self::EXTENSION);

        $iterator = new DirectoryIterator($directory);

        foreach ($iterator as $value) {
            if ($value->isFile() && $value->getExtension() == self::EXTENSION) {
                $this->loadFile($value->getRealPath());
            }
        }
    }

    /**
     * Загружаем большой файл
     *
     * @param string $filename
     */
    protected function loadFile($filename)
    {
        $xml = new XMLReader();
        $xml->open($filename);

        while ($xml->read()) {
            if ($xml->name == "offer") {
                $this->mapped($xml);
            }
        }

        $xml->close();
    }

    /**
     * Разбираем найденный раздел
     *
     * @param $xml
     */
    protected function mapped(XMLReader $xml)
    {
        $offer = simplexml_load_string($xml->readOuterXml());

        $price = $offer->price->__toString();

        if (!empty($price)) {
            $id = $offer->attributes()->id->__toString();
            $id = (int)str_replace('auto-', '', $id);

            $maker = $offer->brandcars->__toString();
            $model = $offer->modelcars->__toString();
            $part = $offer->name->__toString();
            $code = trim($offer->ncatalog->__toString());

            // создаём или обновляем позицию в прайсе
            $product = $this->updateProduct($id, $maker, $model, $part, $code, $price);


            $listImage = $offer->picture->xpath('a');

            if (is_array($listImage) && sizeof($listImage) > 0) {
                foreach ($listImage as $value) {
                    $array = (array)$value;
                    $href = $array["@attributes"]["href"];
                    $this->updateImage($product, $href);
                }
            }
        }
    }

    /**
     * @param int $id
     * @param string $maker
     * @param string $model
     * @param string $part
     * @param string $code
     * @param string $price
     * @return Product
     */
    protected function updateProduct($id, $maker, $model, $part, $code, $price)
    {
        $shopId = 1;

        $product = Product::where('sku', $id)
            ->where('shop_id', $shopId)
            ->first();

        if ($product !== null) {
            if ($product->price != $price) {
                $product->price = $price;
                $product->save();
            }

            return $product;
        }

        $data = [
            'shop_id' => $shopId,
            'sku' => $id,
            'maker' => $maker,
            'model' => $model,
            'part' => $part,
            'code' => $code,
            'price' => $price
        ];

        $product = Product::forceCreate($data);

        return $product;
    }

    protected function updateImage(Product $product, $url)
    {
        if (empty($url)) {
            return;
        }

        // создаём имя
        $imageUtil = new ImageUtil();
        $hash = $imageUtil->createName($url, true);

        // существует в базе
        $image = Image::where('hash', $hash)->first();

        // если нет, добавляем
        if ($image === null) {
            $image = new Image();
            $image->hash = $hash;
            $image->save();
        }

        // сохраняем картинку
        $status = $this->saveImage($hash, $url);

        if (!$status) {
            return;
        }

        // существует привязка к товару
        $productImage = ProductImage::where('product_id', $product->id)
            ->where('image_id', $image->id)
            ->first();

        if ($productImage === null) {
            $productImage = new ProductImage();
            $productImage->product_id = $product->id;
            $productImage->image_id = $image->id;
            $productImage->save();
        }
    }

    protected function saveImage($hash, $url)
    {
        // создаём имя
        $imageUtil = new ImageUtil();

        $filename = base_path('public_html') . $imageUtil->createUrl($hash, false);

        // файл существует
        if (file_exists($filename)) {
            return true;
        }

        $content = @file_get_contents($url);

        // не удалось загрузить
        if (empty($content)) {
            return false;
        }

        // задержка перед загрузкой
        sleep(1);

        // сохраняем изображение
        $imagine = new Imagine();
        $thumbnail = $imagine->load($content);

        // средний размер
        $directory = dirname($filename);

        if (realpath($directory) === false) {
            mkdir($directory, 0755, true);
        }

        $thumbnail->thumbnail(
            $imageUtil->getMiddleBox(),
            ImageInterface::THUMBNAIL_INSET
        )->save($filename);

        // минимальный
        $filename =  base_path('public_html') . $imageUtil->createUrl($hash, true);
        $directory = dirname($filename);

        if (realpath($directory) === false) {
            mkdir($directory, 0755, true);
        }

        $thumbnail->thumbnail(
            $imageUtil->getSmallBox(),
            ImageInterface::THUMBNAIL_INSET
        )->save($filename);

        return true;
    }
}
