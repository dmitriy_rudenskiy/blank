<?php
namespace App\Components\Twig;

use Twig_Extension;
use Twig_SimpleFunction;
use Illuminate\Container\Container;
use Illuminate\Contracts\Auth\Factory as AuthFactory;

class Extension extends Twig_Extension
{
    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'App_Components_Twig_Extension';
    }

    /**Twig
     * {@inheritDoc}
     */
    public function getFunctions()
    {
        return [
            new Twig_SimpleFunction('auth_check', function() {
                return Container::getInstance()->make(AuthFactory::class)->check();
            }),

            new Twig_SimpleFunction('auth_guest', function() {
                return Container::getInstance()->make(AuthFactory::class)->guest();
            }),

            new Twig_SimpleFunction('auth_user', function() {
                return Container::getInstance()->make(AuthFactory::class)->user();
            }),

            new Twig_SimpleFunction('session_has', function($key) {
                return Container::getInstance()->make('session')->has($key);
            }),

            new Twig_SimpleFunction('session_get', function($key) {
                $value = Container::getInstance()->make('session')->get($key);
                Container::getInstance()->make('session')->forget($key);
                return $value;
            }),

            new Twig_SimpleFunction('csrf_token', function() {
                return Container::getInstance()->make('session')->getToken();
            }),

            new Twig_SimpleFunction('cart', function() {
                return sizeof(Container::getInstance()->make('session')->get('shopping_cart'));
            }),

            new Twig_SimpleFunction('route', function($name, $parameters = [], $absolute = true) {
                return Container::getInstance()->make('url')->route($name, $parameters, $absolute);
            }),
        ];
    }
}