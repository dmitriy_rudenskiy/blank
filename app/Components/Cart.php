<?php
namespace App\Components;

use App\Entities\Product;

class Cart
{
    const SESSION_KEY = 'shopping_cart';

    public function get()
    {
        return (array)session(self::SESSION_KEY);
    }

    public function add(Product $product)
    {
        $id = (int)$product->id;

        $cart = $this->get();

        if (!isset($cart[$id])) {
            $cart[$id] = 1;
        }

        session()->put(self::SESSION_KEY, $cart);
    }

    public function remove(Product $product)
    {
        $id = (int)$product->id;

        $cart = $this->get();

        if (isset($cart[$id])) {
            unset($cart[$id]);
        }

        session()->put(self::SESSION_KEY, $cart);
    }

    public function clear()
    {

    }
}