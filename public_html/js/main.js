$(function() {
    $("img.lazy").lazyload();
});

function sendRequest(query) {
    var url = "http://speller.yandex.net/services/spellservice.json/checkText";

    var params = {
        "lang": "ru,en",
        "text":query
    };

    $.get(url, params, function(data) {

        var newQuery = '';

        // нет ошибок
        if (data.length < 1) {
            document.location.href = '/search?q=' + query;
            return;
        }

        $.each(data, function(){
            if ($(this)[0]["s"].length > 0) {
                newQuery = query.replace($(this)[0]["word"], $(this)[0]["s"][0]);
            }
        });

        document.location.href = '/search?q=' + newQuery + '&e=' + query;
    });
}

$(function() {
    $('.wrapper-search button').on('click', function(){
        var input = $(this).parents('div').find('input');
        var query = input.val();

        if (query.length < 3) {
            return;
        }

        sendRequest(query);
    });

    $('.wrapper-search input').keyup(function(e) {

        e = e || window.event;

        console.log(e.keyCode);

        if (e.keyCode === 13) {
            $('.wrapper-search button').click();
        }
    });
});


// slider
$(function() {
    var thumbs = jQuery('#thumbnails').slippry({
        slippryWrapper: '<div class="slippry_box thumbnails" />',
        transition: 'horizontal',
        pager: false,
        auto: false,
        preload: 'visible',
        autoHover: false,
        onSlideBefore: function (el, index_old, index_new) {
            jQuery('.thumbs a img').removeClass('active');
            jQuery('img', jQuery('.thumbs a')[index_new]).addClass('active');
        }
    });

    jQuery('.thumbs a').click(function () {
        thumbs.goToSlide($(this).data('slide'));
        return false;
    });
});

// show product
$(function() {
    $('.product-item').on('click', function(){
        var id = $(this).attr('data-id') * 1;

        document.location.href = '/catalog/price/' + id;
    });
});
